<?php

namespace App\Console\Commands;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Illuminate\Console\Command;

class SeedItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed-items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds items';

    /**
     * @var ItemRepository
     */
    protected $repository;

    /**
     * Create a new config cache command instance.
     *
     * @return void
     */
    public function __construct(ItemRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->itemGenerator() as $item) {
            $this->repository->create(new ItemName($item['name']), new ItemAmount($item['amount']));
        }
    }

    public function itemGenerator()
    {
        for ($i = 1; $i <= 10; $i++) {
            yield [
                'name' => 'Product ' . $i,
                'amount' => rand(0, 20)
            ];
        }
    }
}
