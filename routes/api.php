<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/items', 'Api\Items\ItemsGetAction');
Route::get('/items/{id}', 'Api\Items\ItemGetAction');
Route::post('/items', 'Api\Items\ItemCreateAction');
Route::put('/items/{id}', 'Api\Items\ItemUpdateAction');
Route::delete('/items/{id}', 'Api\Items\ItemDeleteAction');
