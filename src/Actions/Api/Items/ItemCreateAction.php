<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemCreateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateCreateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemCreateJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ItemCreateAction
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ItemValidateCreateService
     */
    protected $validateService;

    /**
     * @var ItemCreateService
     */
    protected $createService;

    /**
     * @var ItemCreateJsonResponder
     */
    protected $responder;

    public function __construct(
        Request $request,
        ItemValidateCreateService $validateService,
        ItemCreateService $createService,
        ItemCreateJsonResponder $responder
    ) {
        $this->request = $request;
        $this->validateService = $validateService;
        $this->createService = $createService;
        $this->responder = $responder;
    }

    public function __invoke(): JsonResponse
    {
        $item = null;

        $params = $this->request->all();

        $errors = $this->validateService->run($params);

        if (empty($errors)) {
            $itemName = new ItemName($params['name']);
            $itemAmount = new ItemAmount($params['amount']);

            $item = $this->createService->run($itemName, $itemAmount);
        }

        return $this->responder->respond($errors, $item);
    }
}
