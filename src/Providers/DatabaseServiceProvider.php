<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class DatabaseServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
    }
}
