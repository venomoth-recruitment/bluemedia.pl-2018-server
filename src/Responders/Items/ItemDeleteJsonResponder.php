<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Responders\Items;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;

class ItemDeleteJsonResponder
{
    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function respond(array $errors): JsonResponse
    {
        if (!empty($errors)) {
            return $this->responseFactory->json(['errors' => $errors], 422);
        }

        return $this->responseFactory->json([], 200);
    }
}
