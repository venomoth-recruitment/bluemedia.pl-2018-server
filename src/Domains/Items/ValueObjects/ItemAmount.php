<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects;

final class ItemAmount
{
    public const MIN_VALUE = 0;
    public const MAX_VALUE = 2147483647;

    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        $this->validateValue($value);

        $this->value = $value;
    }

    private function validateValue($value)
    {
        if ($value < self::MIN_VALUE) {
            throw new \InvalidArgumentException('Value is too low. Minimum value: '. self::MIN_VALUE);
        }

        if ($value > self::MAX_VALUE) {
            throw new \InvalidArgumentException('Value is too high. Maximum value: ' . self::MAX_VALUE);
        }
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
