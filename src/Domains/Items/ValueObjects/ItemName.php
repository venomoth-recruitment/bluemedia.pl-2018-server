<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects;

final class ItemName
{
    public const MIN_LENGTH = 2;
    public const MAX_LENGTH = 255;

    /**
     * @var string
     */
    private $value;

    public function __construct(string $value)
    {
        $this->validateValue($value);

        $this->value = $value;
    }

    private function validateValue($value)
    {
        if (strlen($value) < self::MIN_LENGTH) {
            throw new \InvalidArgumentException('Value is too short. Minimum length: ' . self::MIN_LENGTH);
        }

        if (strlen($value) > self::MAX_LENGTH) {
            throw new \InvalidArgumentException('Value is too long. Maximum length: ' . self::MAX_LENGTH);
        }
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
