<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Illuminate\Validation\Factory;

class ItemValidateCreateService
{
    /**
     * @var Factory
     */
    protected $validationFactory;

    /**
     * @var array
     */
    protected $messages;

    public function __construct(Factory $validationFactory, array $messages = [])
    {
        $this->validationFactory = $validationFactory;
        $this->messages = $messages;
    }

    public function run(array $params): array
    {
        $validator = $this->validationFactory->make($params, [
            'name' => [
                'required',
                'string',
                'min:' . ItemName::MIN_LENGTH,
                'max:' . ItemName::MAX_LENGTH
            ],
            'amount' => [
                'required',
                'integer',
                'min:' . ItemAmount::MIN_VALUE,
                'max:' . ItemAmount::MAX_VALUE
            ]
        ], $this->messages);

        return $validator->errors()->toArray();
    }
}
