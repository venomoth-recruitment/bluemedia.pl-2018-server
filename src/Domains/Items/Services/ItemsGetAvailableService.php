<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Illuminate\Support\Collection;

class ItemsGetAvailableService
{
    /**
     * @var ItemRepository
     */
    protected $repository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->repository = $itemRepository;
    }

    public function run(): Collection
    {
        return $this->repository->available();
    }
}
