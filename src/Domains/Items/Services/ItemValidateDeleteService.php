<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Illuminate\Validation\Factory;

class ItemValidateDeleteService
{
    /**
     * @var Factory
     */
    protected $validationFactory;

    /**
     * @var array
     */
    protected $messages;

    public function __construct(Factory $validationFactory, array $messages = [])
    {
        $this->validationFactory = $validationFactory;
        $this->messages = $messages;
    }

    public function run(array $params): array
    {
        $validator = $this->validationFactory->make($params, [
            'id' => [
                'required',
                'integer',
                'min:' . ItemId::MIN_VALUE,
                'max:' . ItemId::MAX_VALUE,
                'exists:items'
            ],
        ], $this->messages);

        return $validator->errors()->toArray();
    }
}
