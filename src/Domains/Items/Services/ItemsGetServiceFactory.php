<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;

class ItemsGetServiceFactory
{
    public function make(?string $type, ItemRepository $repository)
    {
        switch ($type) {
            case 'available':
                $object = new ItemsGetAvailableService($repository);
                break;
            case 'unavailable':
                $object = new ItemsGetUnavailableService($repository);
                break;
            case 'more_than_five':
                $object = new ItemsGetMoreThanFiveService($repository);
                break;
            default:
                $object = new ItemsGetAvailableService($repository);
        }

        return $object;
    }
}
