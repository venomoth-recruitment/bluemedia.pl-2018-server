<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories;

use App\Models\Item as ItemModel;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Illuminate\Support\Collection;

class EloquentItemRepository implements ItemRepository
{
    /**
     * @var ItemModel
     */
    protected $model;

    public function __construct(ItemModel $model)
    {
        $this->model = $model;
    }

    public function find(ItemId $itemId): ?Item
    {
        $itemModel = $this->model->find($itemId->getValue());

        if ($itemModel === null) {
            return null;
        }

        $itemName = new ItemName($itemModel->name);
        $itemAmount = new ItemAmount($itemModel->amount);

        return new Item($itemId, $itemName, $itemAmount);
    }

    public function create(ItemName $itemName, ItemAmount $itemAmount): Item
    {
        $item = $this->model->create([
            'name' => $itemName->getValue(),
            'amount' => $itemAmount->getValue()
        ]);

        $itemId = new ItemId($item->id);

        return new Item($itemId, $itemName, $itemAmount);
    }

    public function createWithId(Item $item): void
    {
        $this->model->create([
            'id' => $item->getId()->getValue(),
            'name' => $item->getName()->getValue(),
            'amount' => $item->getAmount()->getValue()
        ]);
    }

    public function update(Item $item): bool
    {
        $itemModel = $this->model->find($item->getId()->getValue());

        if ($itemModel === null) {
            return false;
        }

        $itemModel->name = $item->getName()->getValue();
        $itemModel->amount = $item->getAmount()->getValue();
        $itemModel->save();

        return true;
    }

    public function remove(ItemId $itemId): bool
    {
        $itemModel = $this->model->find($itemId->getValue());

        if ($itemModel === null) {
            return false;
        }

        $itemModel->delete();

        return true;
    }

    public function available(): Collection
    {
        return $this->model->where('amount', '>', 0)->get();
    }

    public function unavailable(): Collection
    {
        return $this->model->where('amount', 0)->get();
    }

    public function moreThanFive(): Collection
    {
        return $this->model->where('amount', '>', 5)->get();
    }
}
