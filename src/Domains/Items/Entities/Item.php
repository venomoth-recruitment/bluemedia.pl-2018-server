<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Illuminate\Contracts\Support\Arrayable;

class Item implements Arrayable
{
    /**
     * @var ItemId
     */
    private $id;

    /**
     * @var ItemName
     */
    private $name;

    /**
     * @var ItemAmount
     */
    private $amount;

    public function __construct(ItemId $id, ItemName $name, ItemAmount $amount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->amount = $amount;
    }

    public function getId(): ItemId
    {
        return $this->id;
    }

    public function getName(): ItemName
    {
        return $this->name;
    }

    public function setName(ItemName $name): Item
    {
        $this->name = $name;
        return $this;
    }

    public function getAmount(): ItemAmount
    {
        return $this->amount;
    }

    public function setAmount(ItemAmount $amount): Item
    {
        $this->amount = $amount;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id->getValue(),
            'name' => $this->name->getValue(),
            'amount' => $this->amount->getValue()
        ];
    }
}
