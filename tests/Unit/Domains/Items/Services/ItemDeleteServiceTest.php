<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemDeleteService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use PHPUnit\Framework\TestCase;

class ItemDeleteServiceTest extends TestCase
{
    public function testIfRepositoryValueIsReturned()
    {
        $itemId = new ItemId(1);

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->method('remove')
            ->willReturn(true, false);

        $itemDeleteService = new ItemDeleteService($repositoryMock);

        $this->assertSame(true, $itemDeleteService->run($itemId));
        $this->assertSame(false, $itemDeleteService->run($itemId));
    }

    public function testIfItemIdIsPassedToRepository()
    {
        $itemId = new ItemId(1);

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->expects($this->once())
            ->method('remove')
            ->with($this->equalTo($itemId));

        $itemDeleteService = new ItemDeleteService($repositoryMock);

        $itemDeleteService->run($itemId);
    }
}
