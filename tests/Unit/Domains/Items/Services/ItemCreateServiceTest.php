<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemCreateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;

class ItemCreateServiceTest extends TestCase
{
    public function testIfRepositoryValueIsReturned()
    {
        $itemName = new ItemName('foo');
        $itemAmount = new ItemAmount(10);

        $item = new Item(
            new ItemId(1),
            $itemName,
            $itemAmount
        );

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->method('create')
            ->willReturn($item);

        $itemCreateService = new ItemCreateService($repositoryMock);

        $this->assertSame($item, $itemCreateService->run($itemName, $itemAmount));
    }

    public function testIfItemNameAndItemAmountArePassedToRepository()
    {
        $itemName = new ItemName('foo');
        $itemAmount = new ItemAmount(10);

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->expects($this->once())
            ->method('create')
            ->with($this->equalTo($itemName), $this->equalTo($itemAmount));

        $itemCreateService = new ItemCreateService($repositoryMock);

        $itemCreateService->run($itemName, $itemAmount);
    }
}
