<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateUpdateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Factory;
use Orchestra\Testbench\TestCase;

class ItemValidateUpdateServiceTest extends TestCase
{
    use RefreshDatabase;

    private const ID_MIN_VALUE = 1;
    private const ID_MAX_VALUE = 2147483647;

    private const NAME_MIN_LENGTH = 2;
    private const NAME_MAX_LENGTH = 255;

    private const AMOUNT_MIN_VALUE = 0;
    private const AMOUNT_MAX_VALUE = 2147483647;

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testRequiredParams()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['required' => $expectedErrorMessage]);
        $errors = $itemValidateUpdateService->run([]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertSame($expectedErrorMessage, $errors['name'][0]);

        $this->assertArrayHasKey('amount', $errors);
        $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
    }

    public function testValidateIdAndNameAndAmountOnly()
    {
        $itemId = new ItemId(1);
        $itemName = new ItemName('foo');
        $itemAmount = new ItemAmount(1);

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item($itemId, $itemName, $itemAmount)
        );

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class));
        $this->assertEmpty($itemValidateUpdateService->run([
            'id' => $itemId->getValue(),
            'name' => $itemName->getValue(),
            'amount' => $itemAmount->getValue()
        ]));
    }

    /**
     * @dataProvider idIsIntegerValidationProvider
     */
    public function testIdIsIntegerValidation($id, bool $expectErrors)
    {
        $expectedErrorMessage = 'test fail!';

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item(
                new ItemId(1),
                new ItemName('foo'),
                new ItemAmount(1)
            )
        );

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['integer' => $expectedErrorMessage]);
        $errors = $itemValidateUpdateService->run([
            'id' => $id
        ]);

        if ($expectErrors) {
            $this->assertArrayHasKey('id', $errors);
            $this->assertSame($expectedErrorMessage, $errors['id'][0]);
        } else {
            $this->assertArrayNotHasKey('id', $errors);
        }
    }

    public function idIsIntegerValidationProvider(): array
    {
        return [
            ['foo', true],
            ['1foo', true],
            ['foo1', true],
            [1.1, true],
            [false, true],
            [1, false]
        ];
    }

    public function testIdMinimumMaximumValueValidation()
    {
        $minValue = self::ID_MIN_VALUE;
        $maxValue = self::ID_MAX_VALUE;

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item(
                new ItemId($minValue),
                new ItemName('foo'),
                new ItemAmount(0)
            )
        );
        $itemRepository->createWithId(
            new Item(
                new ItemId($maxValue),
                new ItemName('bar'),
                new ItemAmount(1)
            )
        );

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class));

        $this->assertArrayNotHasKey('id', $itemValidateUpdateService->run(['id' => $minValue]));
        $this->assertArrayNotHasKey('id', $itemValidateUpdateService->run(['id' => $maxValue]));
    }

    public function testIdValueUnderMinimumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['min' => $expectedErrorMessage]);

        $errors = $itemValidateUpdateService->run(['id' => self::ID_MIN_VALUE - 1]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);
    }

    public function testIdValueAboveMaximumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['max' => $expectedErrorMessage]);

        $errors = $itemValidateUpdateService->run(['id' => self::ID_MAX_VALUE + 1]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);
    }

    public function testExistenceValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item(
                new ItemId(1),
                new ItemName('foo'),
                new ItemAmount(1)
            )
        );

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['exists' => $expectedErrorMessage]);

        $this->assertArrayNotHasKey('id', $itemValidateUpdateService->run(['id' => 1]));

        $errors = $itemValidateUpdateService->run(['id' => 2]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);
    }

    /**
     * @dataProvider nameStringValidationProvider
     */
    public function testNameIsStringValidation($name, bool $expectErrors)
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['string' => $expectedErrorMessage]);
        $errors = $itemValidateUpdateService->run([
            'name' => $name
        ]);

        if ($expectErrors) {
            $this->assertArrayHasKey('name', $errors);
            $this->assertSame($expectedErrorMessage, $errors['name'][0]);
        } else {
            $this->assertArrayNotHasKey('name', $errors);
        }
    }

    public function nameStringValidationProvider(): array
    {
        return [
            [111, true],
            [-111, true],
            [0.111, true],
            [-0.111, true],
            [true, true],
            [false, true],
            ['foo', false]
        ];
    }

    public function testNameMinimumMaximumValidation()
    {
        $minimum = substr(base64_encode(random_bytes(2)), 0, self::NAME_MIN_LENGTH);
        $maximum = substr(base64_encode(random_bytes(200)), 0, self::NAME_MAX_LENGTH);

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class));

        $this->assertArrayNotHasKey('name', $itemValidateUpdateService->run(['name' => $minimum]));
        $this->assertArrayNotHasKey('name', $itemValidateUpdateService->run(['name' => $maximum]));
    }

    public function testNameLengthUnderMinimumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $value = substr(base64_encode(random_bytes(2)), 0, self::NAME_MIN_LENGTH - 1);

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['min' => $expectedErrorMessage]);

        $errors = $itemValidateUpdateService->run(['name' => $value]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertSame($expectedErrorMessage, $errors['name'][0]);
    }

    public function testNameLengthAboveMaximumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $value = substr(base64_encode(random_bytes(200)), 0, self::NAME_MAX_LENGTH + 1);

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['max' => $expectedErrorMessage]);

        $errors = $itemValidateUpdateService->run(['name' => $value]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertSame($expectedErrorMessage, $errors['name'][0]);
    }

    /**
     * @dataProvider AmountIsIntegerValidationProvider
     */
    public function testAmountIsIntegerValidation($amount, bool $expectErrors)
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['integer' => $expectedErrorMessage]);
        $errors = $itemValidateUpdateService->run([
            'amount' => $amount
        ]);

        if ($expectErrors) {
            $this->assertArrayHasKey('amount', $errors);
            $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
        } else {
            $this->assertArrayNotHasKey('amount', $errors);
        }
    }

    public function AmountIsIntegerValidationProvider(): array
    {
        return [
            ['foo', true],
            ['1foo', true],
            ['foo1', true],
            [1.1, true],
            [false, true],
            [1, false]
        ];
    }

    public function testAmountMinimumMaximumValueValidation()
    {
        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class));

        $this->assertArrayNotHasKey('amount', $itemValidateUpdateService->run(['amount' => self::AMOUNT_MIN_VALUE]));
        $this->assertArrayNotHasKey('amount', $itemValidateUpdateService->run(['amount' => self::AMOUNT_MAX_VALUE]));
    }

    public function testAmountValueUnderMinimumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['min' => $expectedErrorMessage]);

        $errors = $itemValidateUpdateService->run(['amount' => self::AMOUNT_MIN_VALUE - 1]);

        $this->assertArrayHasKey('amount', $errors);
        $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
    }

    public function testAmountValueAboveMaximumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateUpdateService = new ItemValidateUpdateService(App::make(Factory::class), ['max' => $expectedErrorMessage]);

        $errors = $itemValidateUpdateService->run(['amount' => self::AMOUNT_MAX_VALUE + 1]);

        $this->assertArrayHasKey('amount', $errors);
        $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
    }
}
