<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateCreateService;
use Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Factory;
use Orchestra\Testbench\TestCase;

class ItemValidateCreateServiceTest extends TestCase
{
    private const NAME_MIN_LENGTH = 2;
    private const NAME_MAX_LENGTH = 255;

    private const AMOUNT_MIN_VALUE = 0;
    private const AMOUNT_MAX_VALUE = 2147483647;

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testRequiredParams()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class), ['required' => $expectedErrorMessage]);
        $errors = $itemValidateCreateService->run([]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertSame($expectedErrorMessage, $errors['name'][0]);

        $this->assertArrayHasKey('amount', $errors);
        $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
    }

    public function testValidateNameAndAmountOnly()
    {
        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class));
        $this->assertEmpty($itemValidateCreateService->run(['name' => 'foo', 'amount' => 1]));
    }

    /**
     * @dataProvider nameStringValidationProvider
     */
    public function testNameIsStringValidation($name, bool $expectErrors)
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class), ['string' => $expectedErrorMessage]);
        $errors = $itemValidateCreateService->run([
            'name' => $name
        ]);

        if ($expectErrors) {
            $this->assertArrayHasKey('name', $errors);
            $this->assertSame($expectedErrorMessage, $errors['name'][0]);
        } else {
            $this->assertArrayNotHasKey('name', $errors);
        }
    }

    public function nameStringValidationProvider(): array
    {
        return [
            [111, true],
            [-111, true],
            [0.111, true],
            [-0.111, true],
            [true, true],
            [false, true],
            ['foo', false]
        ];
    }

    public function testNameMinimumMaximumValidation()
    {
        $minimum = substr(base64_encode(random_bytes(2)), 0, self::NAME_MIN_LENGTH);
        $maximum = substr(base64_encode(random_bytes(200)), 0, self::NAME_MAX_LENGTH);

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class));

        $this->assertArrayNotHasKey('name', $itemValidateCreateService->run(['name' => $minimum]));
        $this->assertArrayNotHasKey('name', $itemValidateCreateService->run(['name' => $maximum]));
    }

    public function testNameLengthUnderMinimumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $value = substr(base64_encode(random_bytes(2)), 0, self::NAME_MIN_LENGTH - 1);

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class), ['min' => $expectedErrorMessage]);

        $errors = $itemValidateCreateService->run(['name' => $value]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertSame($expectedErrorMessage, $errors['name'][0]);
    }

    public function testNameLengthAboveMaximumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $value = substr(base64_encode(random_bytes(200)), 0, self::NAME_MAX_LENGTH + 1);

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class), ['max' => $expectedErrorMessage]);

        $errors = $itemValidateCreateService->run(['name' => $value]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertSame($expectedErrorMessage, $errors['name'][0]);
    }

    /**
     * @dataProvider AmountIsIntegerValidationProvider
     */
    public function testAmountIsIntegerValidation($amount, bool $expectErrors)
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class), ['integer' => $expectedErrorMessage]);
        $errors = $itemValidateCreateService->run([
            'amount' => $amount
        ]);

        if ($expectErrors) {
            $this->assertArrayHasKey('amount', $errors);
            $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
        } else {
            $this->assertArrayNotHasKey('amount', $errors);
        }
    }

    public function AmountIsIntegerValidationProvider(): array
    {
        return [
            ['foo', true],
            ['1foo', true],
            ['foo1', true],
            [1.1, true],
            [false, true],
            [1, false]
        ];
    }

    public function testAmountMinimumMaximumValueValidation()
    {
        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class));

        $this->assertArrayNotHasKey('amount', $itemValidateCreateService->run(['amount' => self::AMOUNT_MIN_VALUE]));
        $this->assertArrayNotHasKey('amount', $itemValidateCreateService->run(['amount' => self::AMOUNT_MAX_VALUE]));
    }

    public function testAmountValueUnderMinimumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class), ['min' => $expectedErrorMessage]);

        $errors = $itemValidateCreateService->run(['amount' => self::AMOUNT_MIN_VALUE - 1]);

        $this->assertArrayHasKey('amount', $errors);
        $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
    }

    public function testAmountValueAboveMaximumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateCreateService = new ItemValidateCreateService(App::make(Factory::class), ['max' => $expectedErrorMessage]);

        $errors = $itemValidateCreateService->run(['amount' => self::AMOUNT_MAX_VALUE + 1]);

        $this->assertArrayHasKey('amount', $errors);
        $this->assertSame($expectedErrorMessage, $errors['amount'][0]);
    }
}
