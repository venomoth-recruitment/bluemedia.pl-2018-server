<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\ValueObjects;

use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;

class ItemNameTest extends TestCase
{
    private const MIN_LENGTH = 2;
    private const MAX_LENGTH = 255;

    public function testMinimumLength()
    {
        $value = substr(base64_encode(random_bytes(2)), 0, self::MIN_LENGTH);

        $itemName = new ItemName($value);

        $this->assertSame($value, $itemName->getValue());
    }

    public function testMaximumLength()
    {
        $value = substr(base64_encode(random_bytes(200)), 0, self::MAX_LENGTH);

        $itemName = new ItemName($value);

        $this->assertSame($value, $itemName->getValue());
    }

    public function testValueUnderMinimumLength()
    {
        $value = substr(base64_encode(random_bytes(2)), 0, self::MIN_LENGTH - 1);

        $this->expectException(\InvalidArgumentException::class);

        new ItemName($value);
    }

    public function testValueAboveMaximumLength()
    {
        $value = substr(base64_encode(random_bytes(200)), 0, self::MAX_LENGTH + 1);

        $this->expectException(\InvalidArgumentException::class);

        new ItemName($value);
    }

    public function testToString()
    {
        $value = 'foo';

        $itemName = new ItemName($value);

        $this->assertSame($value, strval($itemName));
    }
}
