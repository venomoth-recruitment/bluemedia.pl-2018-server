<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\ValueObjects;

use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;

class ItemAmountTest extends TestCase
{
    private const MIN_VALUE = 0;
    private const MAX_VALUE = 2147483647;

    public function testMinimumValue()
    {
        $value = self::MIN_VALUE;

        $itemAmount = new ItemAmount($value);

        $this->assertSame($value, $itemAmount->getValue());
    }

    public function testMaximumValue()
    {
        $value = self::MAX_VALUE;

        $itemAmount = new ItemAmount($value);

        $this->assertSame($value, $itemAmount->getValue());
    }

    public function testValueUnderMinimum()
    {
        $this->expectException(\InvalidArgumentException::class);

        new ItemAmount(self::MIN_VALUE - 1);
    }

    public function testValueAboveMaximum()
    {
        $this->expectException(\InvalidArgumentException::class);

        new ItemAmount(self::MAX_VALUE + 1);
    }

    public function testToString()
    {
        $value = 10;

        $itemAmount = new ItemAmount($value);

        $this->assertSame(strval($value), strval($itemAmount));
    }
}
