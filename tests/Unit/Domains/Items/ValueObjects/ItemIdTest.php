<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\ValueObjects;

use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;

class ItemIdTest extends TestCase
{
    private const MIN_VALUE = 1;
    private const MAX_VALUE = 2147483647;

    public function testMinimumValue()
    {
        $value = self::MIN_VALUE;

        $itemId = new ItemId($value);

        $this->assertSame($value, $itemId->getValue());
    }

    public function testMaximumValue()
    {
        $value = self::MAX_VALUE;

        $itemId = new ItemId($value);

        $this->assertSame($value, $itemId->getValue());
    }

    public function testValueUnderMinimum()
    {
        $this->expectException(\InvalidArgumentException::class);

        new ItemId(self::MIN_VALUE - 1);
    }

    public function testValueAboveMaximum()
    {
        $this->expectException(\InvalidArgumentException::class);

        new ItemId(self::MAX_VALUE + 1);
    }

    public function testToString()
    {
        $value = 10;

        $itemId = new ItemId($value);

        $this->assertSame(strval($value), strval($itemId));
    }
}
