<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items\ItemCreateAction;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemCreateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateCreateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemCreateJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

class ItemCreateActionTest extends TestCase
{
    public function testValidRequest()
    {
        $requestParams = [
            'name' => 'foo',
            'amount' => 1
        ];

        $validateServiceErrors = [];

        $itemName = new ItemName($requestParams['name']);
        $itemAmount = new ItemAmount($requestParams['amount']);

        $item = new Item(new ItemId(1), $itemName, $itemAmount);

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('all')
            ->willReturn($requestParams);

        $validateServiceMock = $this->createMock(ItemValidateCreateService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemCreateService::class);
        $createServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($itemName), $this->equalTo($itemAmount))
            ->willReturn($item);

        $responderMock = $this->createMock(ItemCreateJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors), $this->equalTo($item))
            ->willReturn($jsonResponseMock);

        $itemCreateAction = new ItemCreateAction($requestMock, $validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemCreateAction());
    }

    public function testInvalidRequest()
    {
        $requestParams = [
            'name' => '',
            'amount' => -1
        ];

        $validateServiceErrors = [
            'errors' => [
                'name' => ['error'],
                'amount' => ['error']
            ]
        ];

        $item = null;

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('all')
            ->willReturn($requestParams);

        $validateServiceMock = $this->createMock(ItemValidateCreateService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemCreateService::class);
        $createServiceMock->expects($this->never())
            ->method('run');

        $responderMock = $this->createMock(ItemCreateJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors), $this->equalTo($item))
            ->willReturn($jsonResponseMock);

        $itemCreateAction = new ItemCreateAction($requestMock, $validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemCreateAction());
    }
}
