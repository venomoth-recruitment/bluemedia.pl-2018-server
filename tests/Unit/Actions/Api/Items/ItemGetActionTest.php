<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items\ItemGetAction;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemGetService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateGetService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemGetJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

class ItemGetActionTest extends TestCase
{
    public function testValidRequest()
    {
        $id = 1;

        $requestParams = [];

        $validateServiceErrors = [];

        $itemId = new ItemId($id);

        $item = new Item(
            $itemId,
            new ItemName('foo'),
            new ItemAmount(1)
        );

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('all')
            ->willReturn($requestParams);

        $requestParams['id'] = $id;

        $validateServiceMock = $this->createMock(ItemValidateGetService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemGetService::class);
        $createServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($itemId))
            ->willReturn($item);

        $responderMock = $this->createMock(ItemGetJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors), $this->equalTo($item))
            ->willReturn($jsonResponseMock);

        $itemGetAction = new ItemGetAction($requestMock, $validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemGetAction($id));
    }

    public function testInvalidRequest()
    {
        $id = 1;

        $requestParams = [];

        $validateServiceErrors = [
            'errors' => [
                'id' => ['error']
            ]
        ];

        $item = null;

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('all')
            ->willReturn($requestParams);

        $requestParams['id'] = $id;

        $validateServiceMock = $this->createMock(ItemValidateGetService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemGetService::class);
        $createServiceMock->expects($this->never())
            ->method('run');

        $responderMock = $this->createMock(ItemGetJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors), $this->equalTo($item))
            ->willReturn($jsonResponseMock);

        $itemGetAction = new ItemGetAction($requestMock, $validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemGetAction($id));
    }
}
