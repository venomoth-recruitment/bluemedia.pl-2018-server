<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Responders\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemGetJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\App;
use Orchestra\Testbench\TestCase;

class ItemGetJsonResponderTest extends TestCase
{
    /**
     * @dataProvider responseDataProvider
     */
    public function testResponse(array $errors, ?Item $item, bool $shouldReturnOk200)
    {
        $itemGetJsonResponder = new ItemGetJsonResponder(App::make(ResponseFactory::class));

        $response = $itemGetJsonResponder->respond($errors, $item);

        $this->assertInstanceOf(JsonResponse::class, $response);

        if ($shouldReturnOk200) {
            $content = null === $item ? new \stdClass() : $item->toArray();

            $this->assertSame(200, $response->getStatusCode());
            $this->assertSame(json_encode($content), $response->getContent());
        } else {
            $this->assertSame(422, $response->getStatusCode());
            $this->assertSame(json_encode(['errors' => $errors]), $response->getContent());
        }
    }

    public function responseDataProvider(): array
    {
        $errors = [
            'id' => ['Error']
        ];

        $item = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(1)
        );

        return [
            [$errors, null, false],
            [$errors, $item, false],
            [[], null, true],
            [[], $item, true]
        ];
    }
}
