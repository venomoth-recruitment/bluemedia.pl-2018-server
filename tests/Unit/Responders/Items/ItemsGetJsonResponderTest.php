<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Responders\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemsGetJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Orchestra\Testbench\TestCase;

class ItemsGetJsonResponderTest extends TestCase
{
    /**
     * @dataProvider responseDataProvider
     */
    public function testResponse(Collection $items)
    {
        $itemsGetJsonResponder = new ItemsGetJsonResponder(App::make(ResponseFactory::class));

        $response = $itemsGetJsonResponder->respond($items);

        $this->assertInstanceOf(JsonResponse::class, $response);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame(json_encode($items->toArray()), $response->getContent());
    }

    public function responseDataProvider(): array
    {
        $item = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(0)
        );

        $item2 = new Item(
            new ItemId(2),
            new ItemName('bar'),
            new ItemAmount(1)
        );

        return [
            [new Collection([$item, $item2])],
            [new Collection([$item2, $item])],
            [new Collection([])]
        ];
    }
}
