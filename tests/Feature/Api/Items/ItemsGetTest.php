<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Feature\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Orchestra\Testbench\TestCase;

class ItemsGetTest extends TestCase
{
    use RefreshDatabase;

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testNotEmptyJsonResponse()
    {
        $item = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(1)
        );

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId($item);

        $response = $this->get('/api/items');

        $response->assertStatus(200);
        $response->assertJson([$item->toArray()]);
    }

    public function testEmptyJsonResponse()
    {
        $response = $this->get('/api/items');

        $response->assertStatus(200);
        $response->assertJson([]);
    }
}
