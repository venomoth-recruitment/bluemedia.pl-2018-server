<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Feature\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Orchestra\Testbench\TestCase;

class ItemUpdateTest extends TestCase
{
    use RefreshDatabase;

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testInvalidIdItemId()
    {
        $response = $this->get('/api/items/foo');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['id']);
    }

    /**
     * @dataProvider invalidRequestDataProvider
     */
    public function testInvalidRequest(array $params, array $invalidFields)
    {
        $itemId = new ItemId(1);

        $item = new Item(
            $itemId,
            new ItemName('foo'),
            new ItemAmount(1)
        );

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId($item);

        $response = $this->put('/api/items/1', $params);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors($invalidFields);
    }

    public function invalidRequestDataProvider()
    {
        return [
            [
                ['name' => '', 'amount' => 0],
                ['name']
            ],
            [
                ['name' => 'a', 'amount' => 1],
                ['name']
            ],
            [
                ['name' => 'foo', 'amount' => ''],
                ['amount']
            ],
            [
                ['name' => 'foo', 'amount' => -1],
                ['amount']
            ],
            [
                ['name' => '', 'amount' => ''],
                ['name', 'amount']
            ],
            [
                ['name' => 'a', 'amount' => -1],
                ['name', 'amount']
            ]
        ];
    }

    /**
     * @dataProvider validRequestDataProvider
     */
    public function testValidRequest(array $params, array $expectedJson)
    {
        $itemId = new ItemId(1);

        $item = new Item(
            $itemId,
            new ItemName('foo'),
            new ItemAmount(0)
        );

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId($item);

        $response = $this->put('/api/items/1', $params);

        $response->assertStatus(200);
        $response->assertJson($expectedJson);
    }

    public function validRequestDataProvider()
    {
        return [
            [
                ['name' => 'bar', 'amount' => 1],
                ['id' => 1, 'name' => 'bar', 'amount' => 1]
            ],
            [
                ['name' => 'baz', 'amount' => 5],
                ['id' => 1, 'name' => 'baz', 'amount' => 5]
            ]
        ];
    }

    public function testRequestForNotExistingItem()
    {
        $response = $this->put('/api/items/1', [
            'name' => 'foo',
            'amount' => 1
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['id']);
    }
}
