Recruitment Bluemedia Server
============================

**[!] Package is no longer available on [packagist.org](https://packagist.org) so there is no possibility to install it by Composer.**

[!] Package was renamed from *Blue Services* to *Bluemedia* but code uses *Blue Services* names. It was done because *Bluemedia* is parent company of *Blue Services* and it can be found on the internet.

---

Recruitment Bluemedia Server is a part of recruitment task for Bluemedia company.

It implements REST API that allows manage items.

# Table of Contents

- [Installation](#installation)
- [Configuration](#configuration)
- [Items Parameters](#items-parameters)
- [Seeder](#seeder)
- [Usage](#usage)
- [Client](#client)
- [License](#license)

## Installation

```sh
composer require gaqateq/recruitment-blue-services-server
```

```sh
php artisan migrate
```

## Configuration

There is possible to change default class implementing `Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository`.
Default implementation uses Laravel Eloquent and store data in default database.

To change class implementing item repository interface publish config file using command:

```sh
php artisan vendor:publish --provider="Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider"
```

This will generate recruitmentblueservicesserver.php configuration file in your config directory.

## Items Parameters

Items have three parameters:

- `id`
- `name`
- `amount`

## Seeder

Package offers command to seed some auto-generated items. To run seeder use command `db:seed-items`:
```sh
php artisan db:seed-items
```

## Usage

After installation package is ready to use. Now you can use REST API to manage items. Possible operations:

#### `GET /api/items`

Returns all items if `type` parameter is not specified.

`type` parameter limits the results, possible values:
- `available` - returns available items (with amount > 0)
- `unavailable` - returns unavailable items (with amount = 0)
- `more_than_five` - returns items with amount greater than 5

#### `GET /api/items/{id}`

Returns item specified by id.

#### `POST /api/items`

Adds item.

#### `PUT /api/items/{id}`

Edits item specified by id.

#### `DELETE /api/items/{id}`

Deletes item specified by id.

## Client

As mentioned in the header this package is one part of the task. There is available a Client for this API which is available [here](https://gitlab.com/venomoth-recruitment/bluemedia.pl-2018-client).

## License

Recruitment Bluemedia Server is released under the MIT Licence. See the bundled LICENSE file for details.
